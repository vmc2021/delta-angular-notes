import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CommentsService} from "../../services/comments.service";
import {Comment} from "../../models/Comment";

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
//property
//variable referencing your interface array
comments: Comment[] = [];


//updating comment:
  currentComment: Comment = {
    id: 0,
    name: '',
    email: '',
    body: ''
  }
  //property for our update button
  isEdit: boolean = false;

//"commentService" is a variable
  constructor(private commentService: CommentsService) { }

  ngOnInit(): void {
    this.commentService.getCommments().subscribe(data => {
      console.log(data);
      this.comments = data
    })

  }//end of ngOnInit

  //method
  onSubmitComment(commentToAdd: Comment) {
    this.comments.unshift(commentToAdd);
  }//end of onSubmitComment

  //method onUpdateComment
  onUpdateComment(editComment: Comment) {
    this.comments.forEach(
      (current, index) => {
        if (editComment.id === current.id) {
          this.comments.splice(index, 1);
          this.comments.unshift(editComment);
          this.isEdit = false;
          this.currentComment = {
            id: 0,
            name: '',
            email: '',
            body: ''
          }
        }
              }
    );
  }//end of onUpdateComment

  editComment(comment: Comment) {
    this.currentComment = comment;
    this.isEdit = true;
  }//end of edit comment

  //create a method that is subscribing to th observable delete method in
  //the service
  //has type of our interface Comment
  removeComment(commentToBeDeleted:Comment) {
    if(confirm("Are you sure?")) {
      this.commentService.deleteComment(commentToBeDeleted.id)
      //we subscribe to our Observable
        .subscribe(() => {
          this.comments.forEach((current, index) =>{
            if (commentToBeDeleted.id === current.id) {
              //splice deletes that comment
              this.comments.splice(index, 1);
              //now our logic is talking to our service we can go to html
            }
          })
        })
    }
  }


}

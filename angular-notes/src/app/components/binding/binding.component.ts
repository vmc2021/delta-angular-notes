import { Component, OnInit } from '@angular/core';
import {DCHero} from "../../models/DCHero";

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {
//properties
  characters: DCHero[] = [];
  currentClasses: {} = {}; //empty object. used at the end
  currentStyle: {} = {};//empty object.
  enableAddCharacter: boolean = true;

  constructor() { }

  ngOnInit(): void {
    // add data to the array of characters
    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: '27 Smallville',
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: '150 Main street',
          city: "Metropolis",
          state: "CA"
        },
        img: "../assets/img/720-wonder-woman.jpg",

        balance: 14000000,
        memberSince: new Date("07/11/2005 8:30:00"),
        hide: false
      },
      {
        persona: "Batman",
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43,
        address: {
          street: '27 Smallville',
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/70-batman.jpg",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("01/01/1939 4:30:00"),
        hide: false
      }
    ]; //end of array

    //ngOnInit runs this method
    this.setCurrentClasses();
    this.setCurrentStyle();// assign it to something in html like an element

  }//end of ngOnInit

  //method ng class
  setCurrentClasses() {
    //calling and updating our property currentClasses
    this.currentClasses = {
      'btn-success': this.enableAddCharacter //using class from bootstrap
      //add this to the OnInit
    }
  }
  //example of ngStyle
  setCurrentStyle() {
    //call and update the property currentStyle
    this.currentStyle = {
      "padding-top" : "60px",
      "text-decoration" : "underline"
    }
  }
  //method to toggle the individual character's info
  toggleInfo(character: any) {
    //test it out
    console.log("toggle info clicked");
    character.hide = !character.hide;
    //what is .hide? We need to create a property inside ngOnInit. add this property under memberSince.
    //make sure to add hide property in interface.
  }




}//end of class

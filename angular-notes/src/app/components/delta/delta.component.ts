import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delta',
  templateUrl: './delta.component.html',
  styleUrls: ['./delta.component.css']
})
export class DeltaComponent implements OnInit {
  //PROPERTIES- AN ATTRIBUTE OA A COMPONENT
  //THEY ARE LIKE VARIABLES
  firstName: string ="Delta";
  lastName: string = "Cohort";
  year: number = 2021;




  //called when component is initialized
  // used for dependency injections - connection from the service(s)
  constructor() { }



  //called when component is initialized
  //used for reassigning properties and calling methods
  ngOnInit(): void {
    console.log("Hello from delta");
    console.log(`${this.firstName} ${this.lastName}`);
    this.greeting(); //activate greeting here. ngOnInit calls methods
    //we dont need a return statement here
    //in order to access properties, we need to use the 'this' keyword
  }
  // methods are functions in angular
  // methods - function inside of a component's class
  // no need to add function keyword
  //call greeting method/function inside ng OnInit
  greeting() {
    alert("Hello there," + this.firstName);
    //calling displayLastName to run here
    this.displayLastName();
    alert("The year is" + this.year);
    //note: to access properties in a method, use the 'this' keyword
  }
    displayLastName() {
      alert("Last name is" + this.lastName);
    }


}// end of class

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
//properties/logic
  showForm: boolean = true;

  constructor() { }

  ngOnInit(): void {
    //run this component as false
    this.showForm = false;//default to invisible

  }//end of ngONINit

  //method
  //method will set showForm to true/false
  toggleForm() {//when we click button it will run this method
    console.log("Toggle form button was clicked"); //checking to se if it works
    this.showForm = !this.showForm//going back between true and false. next add *ngIf to the div card card body in html
  }
  //method

  triggerEvent(event: any) {
    console.log(event.type);
  }



}//end of class

import { Component, OnInit } from '@angular/core';
import {MarvelHero} from "../../models/MarvelHero";

@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.css']
})
export class NgmodelComponent implements OnInit {
//properties
  heroes: MarvelHero[] = []//initialize empty
  //create a property for the new character entry

  character: MarvelHero = {
    persona: '',
    firstName: '',
    lastName: '',
    age: 0
  };


  constructor() { }

  ngOnInit(): void {
    //add data
    this.heroes = [
      {
        persona: "Captain America",
        firstName: "Steve",
        lastName: "Rogers",
        age: 77
      },
      {
        persona: "Spider-Man",
        firstName: "Peter",
        lastName: "Parker",
        age: 77
      }
    ]

  }//end of ngOnInit

  //methods go here. add a new character
  addCharacter() {
    //what we need to to do? add to beginning or end of array
    this.heroes.unshift(this.character); //unshift adds character to front of array

    //clear out the form /reset

    this.character = {
      persona: '',
      firstName: '',
      lastName: '',
      age: 0
    }

  }






}//end of class

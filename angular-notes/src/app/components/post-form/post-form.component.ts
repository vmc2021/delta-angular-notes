import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostsService} from "../../services/posts.service";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  //properties
  //we going to post single entry at a time
  //created a new property that's referencing our interface
  //set values to 0 or ''
  post: PostHttpClient ={
    //new post
    id: 0,
    title: '',
    body: ''
  };

  @Output()newPost: EventEmitter<PostHttpClient> = new EventEmitter();

  //crude lesson add more properties(3)
  @Output() updatedPost: EventEmitter<PostHttpClient> = new EventEmitter();
  //add an input. defining 'currentPost'
  @Input() currentPost: PostHttpClient = {
    id: 0,
    title: "",
    body: ""
  };
    // property
    @Input() isEditing: boolean = false;



// inject our dependency

  constructor(private  postService: PostsService) { }

  ngOnInit(): void {
  }//end of ngOnInit

  //methods

  //create a method named addPost() by using the PostService's savePost()
  //and attaching the event emitter

  addPost(title: any, body: any) {
    if (!title || !body) {
      alert("Please complete the form")
  }
  else {
      console.log(title, body);
      //at the very end of exercise
      this.postService.savePost({title,body} as PostHttpClient)
        .subscribe(data => {
          this.newPost.emit(data);
        })

    }
  }
  //create a method called updatePost() that will subscribe
  // to the observable method in our service
    updatePost() {
    console.log("Updating post...");
    //property 'currentPost'
    this.postService.updatePost(this.currentPost)
      .subscribe(data => {
        this.isEditing = false;//reset the button back to 'submit post'
        this.updatedPost.emit(data);
      })
      //clears out the form
      this.currentPost = {
      id: 0,
        title: "",
        body: ""
      }
}




}

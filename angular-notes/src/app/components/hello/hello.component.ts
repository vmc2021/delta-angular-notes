import {Component, OnInit} from "@angular/core";

@Component({ //properties
  //update selector to app-hello
  selector: 'app-hello',
  // template: '<h1>Hello World</h1>'

  //assign files to respective properties
  templateUrl: "hello.component.html",
  styleUrls: ["hello.component.css"],

})
//capitalize component hello
export class HelloComponent implements OnInit{
  ngOnInit() {
  }
}
// add this to app.module.ts file

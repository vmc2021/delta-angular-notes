

import {Component, OnInit} from "@angular/core";

@Component({ //properties
  selector: 'app-user',
  // template: '<h2>Jane Doe</h2>'

  //assign files to respective properties
  templateUrl: "user.component.html",
  styleUrls: ["user.component.css"],

})

export class UserComponent implements OnInit{
  ngOnInit() {
  }
}
// add this to app.module.ts file

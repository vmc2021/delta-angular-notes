//1. create this file create component from scratch using template property
// import the Component library from @angular/core
import {Component, OnInit } from "@angular/core";


// 2. create the selector and template using the @Component decorator
@Component ({
  selector: 'app-fruit',
  templateUrl: "fruit.component.html",
  styleUrls: ["fruit.component.css"]
})

//3. export the component class
export class FruitComponent implements OnInit {
  ngOnInit() {
  }
}

//add this information to app.module.ts

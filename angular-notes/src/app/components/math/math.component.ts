import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  //properties go here
  num1: number = 10;
  num2: number = 5;

  constructor() { }

  ngOnInit(): void {
    //call methods here

    this.sum();
    this.difference();
    this.product()
    this.quotient();
    this.fizzbuzz();


  }
  //create method here below ngOnInit method
  sum() {

    return this.num1 + this.num2;
  }

  difference() {

    return this.num1 - this.num2;
  }

  product() {

    return this.num1 * this.num2;
  }

  quotient() {

    return this.num1 / this.num2;
  }



  //  FIZZBUZZ
//use for loop
  fizzbuzz() {
    for (var i = 1; i <= 100; i++) {
      if (i % 3 === 0 && i % 5 === 0) {
        console.log("FizzBuzz");
      }
      else if (i % 3 === 0) {
        console.log("Fizz");
      }
      else if (i % 5 === 0) {
        console.log("Buzz")
      }
      else {
        console.log(i);
      }
    }
  }
  // fizzbuzz() {
  //   for (var i = 1; i <= 100; i++) {
  //     if (i % 3 === 0 && i % 5 === 0) {
  //       console.log("FizzBuzz");
  //     }
  //     else if (i % 3 === 0) {
  //       console.log("Fizz");
  //     }
  //     else if (i % 5 === 0) {
  //       console.log("Buzz")
  //     }
  //     else {
  //       console.log(i);
  //     }
  //   }
  // }





}



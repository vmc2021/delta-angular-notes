import {Component, OnInit, ViewChild} from '@angular/core';
import {DCHero} from "../../models/DCHero";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  //properties
  characters: DCHero[] = [];
  currentClasses: {} = {}; //empty object. used at the end
  currentStyle: {} = {};//empty object.
  enableAddCharacter: boolean = true;

  // character empty object ....part of the submit lesson Monday March 29th
  character: DCHero = {
    persona: '',
    firstName: '',
    lastName: '',
    age: 0,
    address: {
      street: '',
      city: '',
      state: ''
    },
    img: '',
    isActive: true,
    memberSince: '',
    hide: true
  }

  //SUBMIT LESSON Added on Monday, March 29th
  @ViewChild("characterForm") form: any;
  /*
  Calling ViewChild and passed in the name of our form "characterForm"
  - making this our "Form Identifier"
   */
//added for service lesson from Wed. March 30th
  constructor(private dataService: DataService) { }
  /* DEPENDANCY INJECTION
  private = cannot be used anywhere else, only within this class
  dataService = variable name for our DataService
  DataService =
   */

  ngOnInit(): void {


    //ngOnInit runs this method
    this.setCurrentClasses();
    this.setCurrentStyle();// assign it to something in html like an element

    //access the getCharacters() method method that's. Service Lesson from Wednesday
    this.dataService.getCharacters().subscribe(data => {
      this.characters = data;
    } );
    //whenever we are accessing an Observable method, we need to .subscribe to it.





  }//end of ngOnInit

  //method ng class
  setCurrentClasses() {
    //calling and updating our property currentClasses
    this.currentClasses = {
      'btn-success': this.enableAddCharacter //using class from bootstrap
      //add this to the OnInit
    }
  }
  //example of ngStyle
  setCurrentStyle() {
    //call and update the property currentStyle
    this.currentStyle = {
      "padding-top" : "60px",
      "text-decoration" : "underline"
    }
  }
  //method to toggle the individual character's info
  toggleInfo(character: any) {
    //test it out
    console.log("toggle info clicked");
    character.hide = !character.hide;
    //what is .hide? We need to create a property inside ngOnInit. add this property under memberSince.
    //make sure to add hide property in interface.
  }
  //new method for onSubmit
  // onSubmit({value, valid} : ) then onSubmit({value, valid} : {value: DCHero, valid:boolean}) {}
  onSubmit({value, valid} : {value: DCHero, valid:boolean}) {
    if (!valid) {
      alert("Form is not valid");

    }
    else{
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = false;
      console.log(value)
      //add the new data. update the else statement
      this.characters.unshift(value);
      //reset form /clear out the inputs
      this.form.reset();
    }

  }

}

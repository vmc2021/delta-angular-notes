import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-string-interpolation',
  templateUrl: './string-interpolation.component.html',
  styleUrls: ['./string-interpolation.component.css']
})
export class StringInterpolationComponent implements OnInit {
  //properties

  firstName: string = "";
  lastName: string = "";
  age: number = 0;

  constructor() { }

  ngOnInit(): void {
    //use this for your methods
    this.firstName = "Diana"
    this.lastName = "Prince"
    this.age = 5000;


  }
//method with return or console.log
  sayHello() {
    return `Hello from ${this.firstName} ${this.lastName},
    and i am ${this.age} years old`; // wea re now going to bind properties in html file
  }



}

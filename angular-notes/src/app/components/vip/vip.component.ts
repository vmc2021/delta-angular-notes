import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {
//properties
  members: Member[] = [];
  //bonus
  loadingMembers: boolean = false; //initialize to false


  constructor() { }

  ngOnInit(): void {
    //bonus
    //call setTimeout
    setTimeout(() => {
      this.loadingMembers = true
    }, 5000); //now go to html inside of opening UL


    this.members = [
      {
        firstName: "Alyssa",
        lastName: "Blanco",
        userName: "aBlanco",
        memberNo: 1
      },
      {
        firstName: "Angela",
        lastName: "Alexander",
        userName: "aAlerander",
        memberNo: 2
      },
      {
        firstName: "Jose",
        lastName: "Rivera",
        userName: "jRivera",
        memberNo: 3
      },
      {
        firstName: "Victor",
        lastName: "Moreno",
        userName: "vMoreno",
        memberNo: 4
      },
      {
        firstName: "Stephen",
        lastName: "Guedea",
        userName: "sGuedea",
        memberNo: 5
      }

      ]//end of members array

  }//end of ngOnInit

  //Method




}

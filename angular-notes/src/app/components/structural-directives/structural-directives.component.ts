import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrls: ['./structural-directives.component.css']
})
export class StructuralDirectivesComponent implements OnInit {
  //properties go here
  // create a property name users, reference the User interface
  //"users" is more that one object. It's an array. A collection.
  users: User[] = [];// work on ngOnInit next

  constructor() { }

  ngOnInit(): void {
    //add data
    this.users = [
      {
        firstName: "Clark",
        lastName: "Kent",
        age: 55
      },
      {
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43
        //now transfer data to the component html next step
      }
    ]

  }

}

import {Component, OnInit} from "@angular/core";

@Component({ //properties
             //update selector to app-travel
  selector: 'app-travel',
  //assign files to respective properties
  templateUrl: "travel.component.html",
  styleUrls: ["travel.component.css"],

})
//capitalize component travel
export class TravelComponent implements OnInit{
  ngOnInit() {
  }
}
// add this to app.module.ts file

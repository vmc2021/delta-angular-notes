import { Component, OnInit } from '@angular/core';
import {Statement} from "../../models/Statement";
import {StatementsService} from "../../services/statements.service";

@Component({
  selector: 'app-statements',
  templateUrl: './statements.component.html',
  styleUrls: ['./statements.component.css']
})
export class StatementsComponent implements OnInit {
//properties
  statements: Statement[] = [];


  //add variable
  constructor(private statementService: StatementsService) { }

  ngOnInit(): void {
    //subscribe to your Observable method
    this.statementService.getStatements().subscribe(data => {
      console.log(data);
      this.statements = data
    })

  }

}

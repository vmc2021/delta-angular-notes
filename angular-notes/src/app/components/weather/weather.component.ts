import { Component, OnInit } from '@angular/core';
//your selector is app-weather
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  //properties
  goodWeather: string = "Sunny and cool";
  badWeather: string = "Cold and snow storm";


  constructor() { }

  ngOnInit(): void {
    //call methods here
    // this.displayGoodWeatherMessage();
    this.displayBadWeatherMessage();
  }
  //methods go
  displayGoodWeatherMessage() {
    alert(`Today's weather is ${this.goodWeather}`);
  }
  displayBadWeatherMessage() {
    alert(`Bundle up, charge your phones, because today's
    weather is ${this.badWeather}`);
    //call or activate this alert. Go to ng OnInit
  }


}

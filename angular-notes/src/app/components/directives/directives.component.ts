import { Component, OnInit } from '@angular/core';
import {Athlete} from "../../models/Athlete";

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {
  //properties
  //athlete interface
  athletes: Athlete[] = [];//initialize by leaving it empty. pointing to our Athlete interface

  constructor() { }

  ngOnInit(): void { //"hard coding data"
    //how do we access a property inside of a method? use `this` keyword
    this.athletes = [
      {
        name: "Micheal Jordan",
        team: "Chicago Bulls",
        jerseyNo: 23,
        stillPlaying: false
      },
      {
        name: "LeBron James",
        team: "Lakers",
        jerseyNo: 23,
        stillPlaying: true
      },
      {
        name: "Lionel Messi",
        team: "Barcelona",
        jerseyNo: 10,
        stillPlaying: true
      },
      {
        name: "Alex Morgan",
        team: "Orlando Pride",
        jerseyNo: 13,
        stillPlaying: true
      }
    ]//end of athletes array
  }// end of ngOnInit()
  //Method
  showStatus() {
      return "This athlete is currently playing";
    }


}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommentsService} from "../../services/comments.service";
import {Comment} from "../../models/Comment";
import {PostHttpClient} from "../../models/PostHttpClient";

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {
//property: comments interface: Comment
  //Comment
  comment: Comment = {
    //blank sheet for our user to add data when addComment is activated
    name: "",
    id: 0,
    email: "",
    body: ""
  };
//newComment property assigned as an output, subtype of out interface
  @Output() newComment: EventEmitter<Comment> = new EventEmitter();

  //class exercise: update/edit/delete
  //@Output-decorator
  @Output() updatedComment: EventEmitter<Comment> = new EventEmitter();

  //add an input. defining 'currentPost'
  //initialize input by adding properties
  @Input() currentComment: Comment = {
    id: 0,
    name: "",
    email: "",
    body: ""
    //input is now initialized
  };
  // property
  @Input() isEdit: boolean = false;




//dependency injection of the service. Inside constructor: (private: variable/file name)
  constructor(private commentsService: CommentsService )  { }

  ngOnInit(): void {

  }//end of ngOnInit

  //create method addComment

  addComment(name: any, email: any, body: any) {
    if(!name || !email || !body) {
      alert("Please complete the from")
    }
    else {
      console.log(name, email, body);
      this.commentsService.saveComment({name,email,body} as Comment)
        .subscribe(data => {
          this.newComment.emit(data);
        })
    }
  }//end of addComment


  updateComment() {
    console.log("Updating comment...");
    //property 'currentComment'
    this.commentsService.updateComment(this.currentComment)
      .subscribe(data => {
        this.isEdit = false;//reset the button back to 'submit post'
        this.updatedComment.emit(data);
      })
    //clears out the form
    this.currentComment = {
      id: 0,
      name: "",
      email: "",
      body: ""
    }
  }






}//end of class

import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/Movie";
//import Movie goes here


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
//properties go here
  movies: Movie[] = []



  constructor() { }

  ngOnInit(): void {
    //add movie data here
    this.movies = [
      {
        title: "The Predator",
        yearReleased: 1987,
        actors: "Arnold Schwarzenegger",
        director: "John McTiernan",
        genre: "action",
        rating: "R",
        awards: "Austin Films",
        img: "../assets/img/the-predator-2018.jpg"
      },
      {
        title: "Lion King",
        yearReleased: 1994,
        actors: "Robin Williams",
        director: "Bob",
        genre: "animation",
        rating: "PG" ,
        awards: "Florida Sun",
        img: "../assets/img/lion_king.jpeg"
      },
      {
        title: "End Game",
        yearReleased: 2019,
        actors: "Chris Evans",
        director: "Bob",
        genre: "Superhero",
        rating: "PG",
        awards: "People's Choice",

      },
      {
        title: "Get Out",
        yearReleased: 1983,
        actors: "Daniel Kaluuya",
        director: "Jordan Peele",
        genre: "Thriller",
        rating: "R",
        awards: "San Antonio Magazine",

      },
      {
        title: "Deliver Us from Evil",
        yearReleased: 2014,
        actors: "Arnold",
        director: "Scott Derrickson",
        genre: "horror",
        rating: "R",
        awards: "San Francisco Critic",

      },
      {
        title: "21 Jump Street",
        yearReleased: 1983,
        actors: "Arnold",
        director: "Bob",
        genre: "action",
        rating: "PG",
        awards: "San Diego Daily",

      },
      {
        title: "The Karate Kid",
        yearReleased: 1984,
        actors: "Arnold",
        director: "John G. Avildsen",
        genre: "action",
        rating: "PG",
        awards: "Miami Herald",

      },
      {
        title: "Star Wars: Attack of the Clones",
        yearReleased: 2002,
        actors: "Samuel L. Jackson",
        director: "George Lucas",
        genre: "action",
        rating: "PG",
        awards: "Seattle Times",

      }


    ]//end of movies array


    //ngOnInit runs methods here
    this.addMovie()


  }//end of ngOnInit


  //methods for properties go here
  showMessage(){
    return "This movie has an 'R' rating!"
  }
  showPGMessage(){
    return "This movie has a 'PG' rating!"
  }
  //after method is created, activate it in ngOnInit above
  addMovie(){
    this.movies.push({title: "Avatar", yearReleased: 2009, actors:"Sam Worthington", director: "James Cameron", genre: "Animation", rating: "PG",awards: "Best Animated Film, Best Feature, Movie of the Year",});
  }





}//end of export class

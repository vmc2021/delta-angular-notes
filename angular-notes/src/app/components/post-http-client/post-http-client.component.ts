import { Component, OnInit } from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostsService} from "../../services/posts.service";
//angular module above

@Component({
  selector: 'app-post-http-client',
  templateUrl: './post-http-client.component.html',
  styleUrls: ['./post-http-client.component.css']
})
export class PostHttpClientComponent implements OnInit {


  //property empty array
  posts: PostHttpClient[] = [];

  //after lunch update/edit lesson add more properties like we did in post-form ts
  selectedPost: PostHttpClient = {
    id: 0,
    title: "",
    body: ""
  }
  //property for our update button
  postEdit: boolean = false;

  //after adding
  constructor(private postsService: PostsService) {
  }


  //fetch the post whn ngOnInit() method is initialized
  ngOnInit(): void {

    //subscribe to our Observable method that's in our PostsService. postsService represents our service

    this.postsService.getPosts().subscribe(data => {
      console.log(data);//reassigned posts to data
      //we get errors in console log
      //once you update app.module.ts/imports and import
      //check console.log to see array with 100 objects
      //take data and render it to webpage
      //use data binding string interpolation
      //after creating elements in html. go back to ts. Assign a return. we are reassigning our property 'post'
      this.posts = data;
    })


  }

  //methods
  //create onSubmitPost() method that will add a new post to the
  //this.posts array
  onSubmitPost(post: PostHttpClient) {
    this.posts.unshift(post);
  }

  //after lunch
  //new method
  onUpdatedPost(editPost: PostHttpClient) {
    this.posts.forEach(
      (current, index) => {
        if (editPost.id === current.id) {
          this.posts.splice(index, 1);
          //whatever you add, it goes to the top of other posts
          this.posts.unshift(editPost);
          this.postEdit = false;
          this.selectedPost = {
            id: 0,
            title: "",
            body: ""
          }
        }
      });
  }

  editPost(post: PostHttpClient) {
    this.selectedPost = post;

    this.postEdit = true;
    //add this to post-http-client-component.html
  }

//create a method that is subscribing to th observable delete method in
  //the service
  //has type of our interface PostHttpClient
  removePost(postToBeDeleted: PostHttpClient) {
    if(confirm("Are you sure?")) {
      this.postsService.deletePost(postToBeDeleted.id)
        //we subscribe to our Observable
        .subscribe(() => {
          this.posts.forEach((current, index) =>{
            if (postToBeDeleted.id === current.id) {
              //splice deletes that post
              this.posts.splice(index, 1);
              //now our logic is talking to our service we can go to html
            }
          })
        })
    }
  }


}

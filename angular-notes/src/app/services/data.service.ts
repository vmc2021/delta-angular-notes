import { Injectable } from '@angular/core';
import {DCHero} from "../models/DCHero";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  //properties
  //initialize the characters property and assign it to an array of our DCHero interface
  characters: DCHero[] = [];

  //used to inject the data service in the
  constructor() {
    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: '27 Smallville',
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/644-superman.jpg.jpg",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address: {
          street: '150 Main street',
          city: "Metropolis",
          state: "CA"
        },
        img: "../assets/img/720-wonder-woman.jpg",

        balance: 14000000,
        memberSince: new Date("07/11/2005 8:30:00"),
        hide: false
      },
      {
        persona: "Batman",
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43,
        address: {
          street: '27 Smallville',
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/70-batman.jpg",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("01/01/1939 4:30:00"),
        hide: false
      }
    ]; //end of array
  }//end of constructor

  //methods go here
  //create a method that will return an array of this.characters
  //using an observable
  /* Observables: provides support for passing messages/data between parts of your application
  Used frequently and is a technique for event handling, asynchronous programing, and handling multiple values
   */
  //method has subtype" : ", Observable has a type DCHero
  getCharacters() : Observable<DCHero[]> {
    //we are getting dataservices
    console.log("Getting characters from dataService");
    //observable needs "of()"
    return of(this.characters);

    //not execute injection to template

  }



}//end of class

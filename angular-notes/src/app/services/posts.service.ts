import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostHttpClient} from "../models/PostHttpClient";

const httpOptions = {
  headers: new HttpHeaders({"Content-Type": "application/json"})
}
//setting up our headers value to the content-type of application/json

@Injectable({
  providedIn: 'root'
})
export class PostsService {
//service class
  //property

  //set Url
  postsUrl: string = "https://jsonplaceholder.typicode.com/posts" //initialize to

  //inject the HttpClientModule as a dependency
  constructor(private httpClient: HttpClient) { }
  //now we can make external requests


  //create method after constructor private
  //name of interface is PostHttpClient
  getPosts() : Observable<PostHttpClient[]> {

    //returning all the data that comes with our postUrl property. Dependecy : httpClient variable that represents dependancy
    //return this.httpClient and get PostHttpClient[]
    return this.httpClient.get<PostHttpClient[]> (this.postsUrl);
  }
//create a method that will make a Post request to the postUrl property
  //post Observable method
  //post is parameter
  savePost(post: PostHttpClient) : Observable<PostHttpClient> {
    return this.httpClient.post<PostHttpClient>(this.postsUrl, post, httpOptions)
  }
  //PUT request update/edit lesson
  //create method that

  updatePost(postUpdated: PostHttpClient) : Observable<PostHttpClient> {
    //created new const var that is taking the json url and the id of the selected post
    const url = `${this.postsUrl}/${postUpdated.id}`;
    //we are returning everything as a PUT request, not as a POST
    //"PUTTING" this on the 'const url' NOT on the 'this.postsUrl
    return this.httpClient.put<PostHttpClient>(url,postUpdated, httpOptions)
  }


  //crude lesson
  //create a method Delete
  //use type of interface
  deletePost(postDeleted: PostHttpClient | number) : Observable<PostHttpClient> {
    const id = typeof postDeleted === 'number' ? postDeleted : postDeleted.id;

    /* if whats being passed in is a typeof number, its postDeleted (our parameter)
    otherwise use the id of 'postDeleted'
     */
    const url = `${this.postsUrl}/${id}`//targeting the id
    return this.httpClient.delete<PostHttpClient>(url, httpOptions);


  }

}//end of service class

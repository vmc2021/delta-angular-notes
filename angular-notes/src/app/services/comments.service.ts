import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Comment} from "../models/Comment";

const httpOptions = {
  headers: new HttpHeaders({"Content-Type": "application/json"})
}

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
//property

  //url commentsUrl as a property
  commentsUrl: string = "https://jsonplaceholder.typicode.com/comments";

  //inject the
  constructor(private httpClient: HttpClient) {
  }

  //create a method that will make our Get request to the commentsUrl

getCommments() : Observable<Comment[]> {
    return this.httpClient.get<Comment[]>(this.commentsUrl)
}
//saveComment method

  //post:adds data to the api. post has three parameters Url, parameter, type
  saveComment(comment: Comment) : Observable<Comment> {
    return this.httpClient.post<Comment>(this.commentsUrl,comment,httpOptions)
  }

  //update
  updateComment(commentUpdated: Comment) : Observable<Comment> {
    const url = `${this.commentsUrl}/${commentUpdated}`;
    return this.httpClient.put<Comment>(url,Comment, httpOptions)
  }
  //delete method
  deleteComment(commentDeleted: Comment | number) : Observable<Comment> {
    const id = typeof commentDeleted === 'number' ? commentDeleted : commentDeleted.id;

    const url = `${this.commentsUrl}/${id}`
    return this.httpClient.delete<Comment>(url,httpOptions);
  }





}

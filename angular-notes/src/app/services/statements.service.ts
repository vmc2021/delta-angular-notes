import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Statement} from "../models/Statement";

@Injectable({
  providedIn: 'root'
})
export class StatementsService {
//properties

  //url
  statementsUrl: string = "https://jsonplaceholder.typicode.com/comments";


//inject the HttpClient
  constructor(private httpClient: HttpClient) {

  }
  //create a method that will make our Get request to the statementUrl
  getStatements() : Observable<Statement[]> {
  return this.httpClient.get<Statement[]>(this.statementsUrl)
  }


}

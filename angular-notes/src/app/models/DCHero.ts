export interface DCHero {
  persona: string,
  firstName: string,
  lastName: string,
  age: number,
  address?: {
    street?: string,
    city?: string,
    state?: string
  },
  img?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any,
  hide?: boolean
}
//Note: ? properties with a ? can be removed from ts file because they are optional. It woun't create conflicts

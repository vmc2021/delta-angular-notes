export interface Statement {
  id: number,
  name: string,
  email: string,
  body: string
}


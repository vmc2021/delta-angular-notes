export interface User {
  firstName: string,
  lastName: string,
  age: number,
  //Note: - makes the property optional / not required
  email?: string,
  image?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any
}
